/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * http://www.scm-manager.com
 * 
 */

package de.triology.scm.plugins.htpasswd;

import java.io.File;
import java.io.IOException;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.SCMContextProvider;
import sonia.scm.plugin.ext.Extension;
import sonia.scm.store.Store;
import sonia.scm.store.StoreFactory;
import sonia.scm.user.UserManager;
import sonia.scm.util.AssertUtil;
import sonia.scm.web.security.AuthenticationHandler;
import sonia.scm.web.security.AuthenticationResult;

import com.google.inject.Inject;

/**
 * Class for Htpasswd authentication handler.
 * 
 * @author Jan Boerner, Ahmed Saad, TRIOLOGY GmbH
 */
@Singleton
@Extension
public class HtpasswdAuthenticationHandler implements AuthenticationHandler {

	/** the logger for HtpasswdAuthenticationHandler. */
	private static final Logger LOGGER = LoggerFactory.getLogger(HtpasswdAuthenticationHandler.class);

	/** Storage identifier. */
	public static final String STORE_NAME = "htpasswd-auth";

	/** The configuration of this plugin. */
	private HtpasswdConfig config;

	/** Htpasswd functions. */
	private final Htpasswd htpasswd = new Htpasswd();

	/** Storage system holding configuration of this plugin. */
	private final Store<HtpasswdConfig> store;

	/** The .htpasswd file. */
	private File htpasswdFile;

	/** The path of .htpasswd file. */
	private String htpasswdPath;

	/** Time stamp of the last change of the file. */
	private long changeTimeStamp;

	/** UserManager. */
	private final UserManager userManager;

	/**
	 * Constructs ...
	 * 
	 * @param storeFactory StoreFactory
	 * @param pUserManager UserManager
	 */
	@Inject
	public HtpasswdAuthenticationHandler(final StoreFactory storeFactory, final UserManager pUserManager) {
		store = storeFactory.getStore(HtpasswdConfig.class, STORE_NAME);
		userManager = pUserManager;
	}

	/**
	 * Authenticate.
	 * 
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @param username User name
	 * @param password Password
	 * 
	 * @return Authentication Result
	 */
	@Override
	public final AuthenticationResult authenticate(final HttpServletRequest request,
			final HttpServletResponse response, final String username, final String password) {
		AssertUtil.assertIsNotEmpty(username);
		AssertUtil.assertIsNotEmpty(password);

		if (!htpasswdPath.equals(config.getHtpasswdFilepath())) {
			htpasswdPath = config.getHtpasswdFilepath();
			htpasswdFile = new File(htpasswdPath);
			changeTimeStamp = htpasswdFile.lastModified();
			htpasswd.readHtpasswd(htpasswdFile);

		} else if (htpasswdFile.lastModified() != changeTimeStamp) {
			changeTimeStamp = htpasswdFile.lastModified();
			htpasswd.readHtpasswd(htpasswdFile);
		}
		if (!new File(htpasswdPath).isFile()) {
			htpasswd.clearDB();
		}
		htpasswd.setUserManager(userManager);

		return htpasswd.authenticate(username, password);
	}

	/**
	 * Close.
	 * 
	 * @throws IOException Exception
	 */
	@Override
	public void close() throws IOException {

	}

	/**
	 * Initialize authentication configuration.
	 * 
	 * @param context SCMContextProvider
	 */
	@Override
	public final void init(final SCMContextProvider context) {
		config = store.get();

		if (config == null) {
			config = new HtpasswdConfig();
		}

		htpasswdPath = config.getHtpasswdFilepath();
		htpasswdFile = new File(htpasswdPath);
		changeTimeStamp = htpasswdFile.lastModified();

		htpasswd.readHtpasswd(htpasswdFile);
	}

	/**
	 * Set configuration in store.
	 * 
	 */
	public final void storeConfig() {
		store.set(config);
	}

	// ~--- get methods ----------------------------------------------------------

	/**
	 * Get Htpasswd Configuration.
	 * 
	 * @return HtpasswdConfig
	 */
	public final HtpasswdConfig getConfig() {
		return config;
	}

	/**
	 * Get Type.
	 * 
	 * @return Type as string
	 */
	@Override
	public final String getType() {
		return htpasswd.getType();
	}

	// ~--- set methods ----------------------------------------------------------

	/**
	 * Set Htpasswd configuration.
	 * 
	 * @param pConfig Htpasswd configuration
	 */
	public final void setConfig(final HtpasswdConfig pConfig) {
		this.config = pConfig;
	}
}
