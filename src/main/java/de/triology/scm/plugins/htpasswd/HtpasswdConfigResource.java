/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */

package de.triology.scm.plugins.htpasswd;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.security.Role;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Configuration of the htpasswd-Authentication-Plugin.
 * 
 * @author Jan Boerner, Ahmed Saad, TRIOLOGY GmbH
 */
@Singleton
@Path("config/auth/htpasswd")
public class HtpasswdConfigResource {

	/** Authentication handler. */
	private final HtpasswdAuthenticationHandler authenticationHandler;

	/** the logger for HtpasswdConfigResource. */
	private static final Logger LOGGER = LoggerFactory.getLogger(HtpasswdConfigResource.class);

	/**
	 * Constructs ...
	 * 
	 * @param pAuthenticationHandler Htpasswd authentication handler
	 */
	@Inject
	public HtpasswdConfigResource(final HtpasswdAuthenticationHandler pAuthenticationHandler) {
		if (!SecurityUtils.getSubject().hasRole(Role.ADMIN)) {
			LOGGER.warn("user has not enough privileges to configure htpasswd");

			throw new WebApplicationException(Status.FORBIDDEN);
		}
		authenticationHandler = pAuthenticationHandler;
	}

	// ~--- get methods ----------------------------------------------------------

	/**
	 * Get configuration.
	 * 
	 * @return Htpasswd configuration
	 */
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public final HtpasswdConfig getConfig() {
		return authenticationHandler.getConfig();
	}

	// ~--- set methods ----------------------------------------------------------

	/**
	 * Set configuration.
	 * 
	 * @param uriInfo UriInfo
	 * @param config HtpasswdConfig
	 * 
	 * @return Response
	 * 
	 * @throws IOException Exception
	 */
	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public final Response setConfig(@Context final UriInfo uriInfo, final HtpasswdConfig config) throws IOException {
		String htpasswdFilepath = config.getHtpasswdFilepath();
		if (new File(htpasswdFilepath).isFile()) {
			authenticationHandler.setConfig(config);
			authenticationHandler.storeConfig();
			return Response.created(uriInfo.getRequestUri()).build();
		} else {
			LOGGER.error("{} File does not exists!", htpasswdFilepath);
			return Response.notAcceptable(null).build();
		}
	}
}
