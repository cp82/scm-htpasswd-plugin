/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */
Ext.ns('Triology.htpasswd');
Triology.htpasswd.GlobalConfigPanel = Ext.extend(Sonia.config.ConfigForm, {

  initComponent : function() {
	var config = {
		title : Triology.htpasswd.I18n.titleText,
		items : [{
		    xtype : 'textfield',
		    name : 'htpasswd-filepath',
		    fieldLabel : Triology.htpasswd.I18n.filePathText,
		    helpText : Triology.htpasswd.I18n.filePathHelpText,
		    allowBlank : false
	    }]
	};

	Ext.apply(this, Ext.apply(this.initialConfig, config));
	Triology.htpasswd.GlobalConfigPanel.superclass.initComponent.apply(this, arguments);
  },
  
  onSubmit: function(values){
    this.el.mask(Triology.htpasswd.I18n.submitText);
    Ext.Ajax.request({
      url: restUrl + 'config/auth/htpasswd.json',
      method: 'POST',
      jsonData: values,
      scope: this,
      disableCaching: true,
      success: function(response){
        this.el.unmask();
      },
      failure: function(){
        this.el.unmask();
		Ext.MessageBox.show({
		    title : Triology.htpasswd.I18n.errorBoxTitle,
		    msg : Triology.htpasswd.I18n.errorOnSubmitText,
		    buttons : Ext.MessageBox.OK,
		    icon : Ext.MessageBox.ERROR
		});
      }
    });
  },

  onLoad: function(el){
    var tid = setTimeout( function(){ el.mask(Triology.htpasswd.I18n.loadingText); }, 100);
    Ext.Ajax.request({
      url: restUrl + 'config/auth/htpasswd.json',
      method: 'GET',
      scope: this,
      disableCaching: true,
      success: function(response){
        var obj = Ext.decode(response.responseText);
        this.load(obj);
        clearTimeout(tid);
        el.unmask();
      },
      failure: function(){
        clearTimeout(tid);
        el.unmask();
		Ext.MessageBox.show({
		    title : Triology.htpasswd.I18n.errorBoxTitle,
		    msg : Triology.htpasswd.I18n.errorOnLoadText,
		    buttons : Ext.MessageBox.OK,
		    icon : Ext.MessageBox.ERROR
		});
      }
    });
  }
});

Sonia.user.FormPanel.prototype.htpasswdIsReadOnly = Sonia.user.FormPanel.prototype.isReadOnly;
Ext.override(Sonia.user.FormPanel, {
    isReadOnly: function(){
    	var readOnly = this.htpasswdIsReadOnly();
    	
    	if(this.item != null && this.item.type == 'htpasswd'){
    		readOnly = !this.htpasswdIsReadOnly();
    	}
    	return readOnly;
	}
});

Ext.reg("htpasswdGlobalConfigPanel", Triology.htpasswd.GlobalConfigPanel);

// register global config panel
registerGeneralConfigPanel({
	id : 'htpasswdGlobalConfigPanel',
	xtype : 'htpasswdGlobalConfigPanel'
});
