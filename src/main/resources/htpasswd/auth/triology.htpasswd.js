/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */
Ext.ns('Triology.htpasswd');

Triology.htpasswd.I18n = {

    titleText : 'Htpasswd Authentication',

    filePathText : '.htpasswd filepath',
    filePathHelpText : 'Path of .htpasswd file.',

    loadingText : 'Loading ...',
    submitText : 'Submit ...',
    
	msgTitleText : 'Message',

    // errors
    errorBoxTitle : 'Error',
    errorOnSubmitText : 'Check the path of your file.',
    errorOnLoadText : 'Error during config load.',
    errorOnReloadText : 'Error during reload.',
};

// German translation

if ('de' == i18n.country) {
    Triology.htpasswd.I18n = {

	titleText : 'Htpasswd Authentifizierung',

	filePathText : 'Pfad der .htpasswd-Datei',
	filePathHelpText : 'Der Pfad der .htpasswd-Datei.',

	loadingText : 'Laden ...',
	submitText : 'Senden ...',
	
	msgTitleText : 'Meldung',

	// errors
	errorBoxTitle : 'Fehler',
	errorOnSubmitText : 'Prüfen Sie ob die angegebene Datei existiert.',
	errorOnLoadText : 'Fehler beim Laden der Konfigurations-Werte.',
    errorOnReloadText : 'Fehler beim Laden der Datei.',
    };
}
